#pragma once
#include <iostream>
using namespace std;


template <typename T> class myList {
	struct Node
	{
		T val;
		Node *next, *prev, *current;
	};

	int size;
	Node *head, *tail;

public:
	myList();
	myList(const myList &list);
	~myList();
	int get_size();
	void clear();
	bool is_empty();
	bool value_is_find(T val);
	T& at(int pos);
	bool change_value(int pos, int value);
	int get_position(T val);
	void push_back(T val);
	bool push_at_position(int pos, int value);
	bool del_at_value(T val);
	bool del_at_position(int pos);
	void show();

	class Iterator 
	{
		Node *iter;
	public:
		Iterator(Node *iter)
		{
			this->iter = iter;
		}
		T &operator *() 
		{
			return iter->val;
		}
		Iterator &operator ++(int)
		{
			Iterator it = *this;
			iter = iter->next;
			return it;
		}
		Iterator &operator --(int)
		{
			Iterator it = *this;
			iter = iter->prev;
			return it;
		}
		bool operator ==(const Iterator& iter2)
		{
			return this->iter == iter2.iter;
		}
		bool operator !=(const Iterator& iter2)
		{
			return this->iter != iter2.iter;
		}
	};
	Iterator begin()
	{
		return Iterator(head);
	}
	Iterator rbegin()
	{
		return Iterator(tail);
	}
	Iterator end()
	{
		return Iterator(tail);
	}
	Iterator rend()
	{
		return Iterator(head);
	}
};

template <typename T> myList<T>::myList()
{
	size = 0;
	head = tail = NULL;
}
template <typename T> myList<T>::myList(const myList &list)
{

}
template <typename T> myList<T>::~myList()
{

}
template <typename T> int myList<T>::get_size()
{
    return size;
}
template <typename T> void myList<T>::clear()
{
    Node *current = tail;
    
    if (size == 1) {
        head = NULL;
        tail = NULL;
        current = NULL;
        size = 0;
    }
    
    if (size > 1) {
            for (int i = 0; i < size; i++) {
                current = tail -> prev;
                tail = NULL;
                tail = current;
                current = tail -> prev;
                if (current == head)
                {
                    head = NULL;
                    tail = NULL;
                    current = NULL;
                    size = 0;
                    
            }
        }
    }
}
template <typename T> bool myList<T>::is_empty()
{
    if (size == 0) {
        return true;
    } else {
        return false;
    }
}

template <typename T> bool myList<T>::value_is_find(T value)
{
    Node *current = tail;
    int tmp = 0;
    bool flag = false;
    
    
    for (int i = 0; i <= size; i++){
        tmp = current->val;
        if (tmp == value){
            flag = true;
        } else {
            current = current -> prev;
            
        }
    }
    return flag;
}

template <typename T> T& myList<T>::at(int pos)
{
    Node *current = tail;
    int i = 1;
    
    while (pos != i) {
        current = current -> prev;
        i++;
    }
    return current -> val;
}
template <typename T> bool myList<T>::change_value(int pos, int value)
{
    Node *current = tail;
    int i = 1;
    
    while (pos != i) {
        current = current -> prev;
        i++;
    }
    current -> val = value;
    return true;
}
    
template <typename T> int myList<T>::get_position(T val)
{
    
    Node *current = tail;
    int tmp = 0;
    int position = 0;
    
    for (int i = 1; i <= size; i++){
        tmp = current->val;
        if (tmp == val){
            position = i;
            break;
        } else {
            current = current -> prev;
        }
    }
    return position;
}

template <typename T> void myList<T>::push_back(T val)
{
	Node *tmp = new Node;
	tmp->val = val;
	if (!head)
	{
		head = tail = tmp;
		tail->next = tmp;
		tail->prev = tmp;
	}
	else
	{
		tmp->next = tail->next;
		tmp->next->prev = tmp;
		tail->next = tmp;
		tmp->prev = tail;
	}
	tail = tmp;
	size++;
}
template <typename T> bool myList<T>::push_at_position(int pos, int value)
{
    Node *current = tail;
    Node *tmp = new Node;
    int i = 1;
    
    while (pos != i) {
        current = current -> prev;
        i++;
    }
    tmp -> prev = current -> prev;
    tmp -> next = current;
    current -> prev = tmp;
    tmp -> prev -> next = tmp;
    tmp->val = value;
    size++;
    return true;
}
template <typename T> bool myList<T>::del_at_value(T val)
{
    Node *current = tail;
    int tmp = 0;

    for (int i = 1; i < size; i++){
        tmp = current->val;
        if (tmp == val){
            current -> prev -> next = current ->next;
            current -> next -> prev = current ->prev;
            current = NULL;
        } else {
            current = current -> prev;
        }
        
    }
    size--;
    return true;
}


template <typename T> bool myList<T>::del_at_position(int pos)
{
    Node *current = tail;
    int i = 1;
    
    while (pos != i) {
        current = current -> prev;
        i++;
    }
    
    
    current -> prev ->next  = current ->next;
    current -> next -> prev = current ->prev;
    if (pos == 1) {
        tail = tail -> prev;
    }
    current = NULL;
    size--;
    return true;
}


template <typename T> void myList<T>::show()
{
	Node *tmp = tail;
	for (int i = 0; i < size; i++)
	{
		cout << tmp->val << endl;
		tmp = tmp->prev;
	}
}
