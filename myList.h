#pragma once
#include <iostream>
using namespace std;


template <typename T> class myList {
    struct Node
    {
        T val;
        Node *next, *prev;
    };

    int size;
    Node *head, *tail;

public:
    myList();
    myList(myList &list);
    ~myList();
    int get_size();
    void clear();
    bool is_empty();
    bool value_is_find(T val);
    T& value_of_position (int pos);
    bool change_value(int pos, T value);
    int get_position(T val);
    void push_back(T val);
    bool push_at_position(int pos, T value);
    bool del_at_value(T val);
    bool del_at_position(int pos);
    void show();

    class Iterator
    {
        Node *iter;
    public:
        Iterator(Node *iter)
        {
            this->iter = iter;
        }
        T &operator *()
        {
            return iter->val;
        }
        Iterator &operator ++(int)
        {
            Iterator it = *this;
            iter = iter->next;
            return it;
        }
        Iterator &operator --(int)
        {
            Iterator it = *this;
            iter = iter->prev;
            return it;
        }
        bool operator ==(const Iterator& iter2)
        {
            return this->iter == iter2.iter;
        }
        bool operator !=(const Iterator& iter2)
        {
            return this->iter != iter2.iter;
        }
    };
    Iterator begin()
    {
        return Iterator(head);
    }
    Iterator rbegin()
    {
        return Iterator(tail);
    }
    Iterator end()
    {
        return Iterator(tail);
    }
    Iterator rend()
    {
        return Iterator(head);
    }
};

template <typename T> myList<T>::myList()
{
    size = 0;
    head = tail = NULL;
}
template <typename T> myList<T>::myList(myList &list)
{
    size = 0;
    head = tail = NULL;
    while (size != list.size)
    {
        push_back(list.at(get_size() + 1));
    }
}
template <typename T> myList<T>::~myList()
{
    while (size != 0)
    {
        Node *tmp = head->next;
        delete head;
        head = tmp;
        size--;
    }
}
template <typename T> int myList<T>::get_size()
{
    return size;
}
template <typename T> void myList<T>::clear()
{
    Node *current = tail;
    
    if (size == 1) {
        head = NULL;
        tail = NULL;
        current = NULL;
        size = 0;
    }
    
    if (size > 1) {
            for (int i = 0; i < size; i++) {
                current = tail -> prev;
                tail = NULL;
                tail = current;
                current = tail -> prev;
                if (current == head)
                {
                    head = NULL;
                    tail = NULL;
                    current = NULL;
                    size = 0;
                    
            }
        }
    }
}
template <typename T> bool myList<T>::is_empty()
{
    if (size == 0) {
        return true;
    } else {
        return false;
    }
}

template <typename T> bool myList<T>::value_is_find(T value)
{
    Node *current = tail;
    
    for (int i = 1; i <= size; i++){
        if (current->val == value){
            return true;
        } else {
            current = current -> prev;
        }
    }
    return false;
}

template <typename T> T& myList<T>::value_of_position(int pos)
{
    //TODO: exception (pos < 1 or pos > size)

    Node *current = head;
    
    for (int i = 1; i != pos; i++) {
        current = current -> next;
    }
    return current -> val;
}
template <typename T> bool myList<T>::change_value(int pos, T value)
{
    if (pos < 1 || pos > size) return false;

    Node *current = head;

    for (int i = 1; i != pos; i++) {
        current = current -> next;
    }
    current -> val = value;
    return true;
}
    
template <typename T> int myList<T>::get_position(T val)
{
    Node *current = head;
    
    for (int pos = 1; pos <= size; pos++){
        if (current->val == val){
            return pos;
        } else {
            current = current -> next;
        }
    }
    return 0;
}

template <typename T> void myList<T>::push_back(T val)
{
    Node *tmp = new Node;
    tmp->val = val;
    if (!head)
    {
        head = tail = tmp;
        tail->next = tmp;
        tail->prev = tmp;
    }
    else
    {
        tmp->next = tail->next;
        tmp->next->prev = tmp;
        tail->next = tmp;
        tmp->prev = tail;
    }
    tail = tmp;
    size++;
}
template <typename T> bool myList<T>::push_at_position(int pos, T value)
{
    if (pos < 1 || pos > size + 1) return false;
    else if (pos == size + 1) {
        push_back(value);
        return true;
    }
    Node *current = head;
    Node *tmp = new Node;
    tmp->val = value;
    
    for (int i = 1; i != pos; i++) {
        current = current -> next;
    }

    tmp->prev = current->prev;
    tmp->next = current;
    current->prev = tmp;
    tmp->prev->next = tmp;
    if (pos == 1) head = tmp;
    size++;
    return true;
}
template <typename T> bool myList<T>::del_at_value(T val)    //1 2 3 4
{
    Node *current = tail;
    for (int i = 1; i <= size; i++){
        if (current->val == val){
            current -> prev -> next = current ->next;
            current -> next -> prev = current ->prev;
            if (i == size) {
                head = current->next;
            }
            else if (i == 1) {
                tail = current->prev;
            }
            current = NULL;
            size--;
            return true;
        } else {
            current = current -> prev;
        }
    }
    return false;
}


template <typename T> bool myList<T>::del_at_position(int pos)
{
    if (pos < 1 || pos > size) return false;

    Node *current = head;
    
    for (int i = 1; i != pos; i++) {
        current = current -> next;
    }
    current -> prev ->next  = current ->next;
    current -> next -> prev = current ->prev;
    if (pos == 1) {
        head = head -> next;
    }
    else if (pos == size) {
        tail = tail->prev;
    }
    current = NULL;
    size--;
    return true;
}


template <typename T> void myList<T>::show()
{
    Node *tmp = head;
    for (int i = 0; i < size; i++)
    {
        cout << tmp->val << endl;
        tmp = tmp->next;
    }
}
