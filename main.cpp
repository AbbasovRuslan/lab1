#include <iostream>
#include "myList.h"
using namespace std;


int main()
{
    myList<int> list;
    myList<int>::Iterator i = list.begin();
    
    int choice = 99;
    int secondChoice =101;
    
    list.push_back(4);
    list.push_back(4);
    list.push_back(6);

    
    do
    {
    system( "read -n 1 -s -p \"Press any key to continue...\"" );
    cout << endl
    << "Операции интерфейса" << endl
    << "1 запросить размер" << endl
    << "2 Очистить " << endl
    << "3 проверить на пустоту" << endl
    << "4 запросить наличие элемента" << endl
    << "5 прочитать значение по номеру" << endl
    << "6 изменить значение по номеру" << endl
    << "7 получить номер элемента по значению" << endl
    << "8 вставить новое значение" << endl
    << "9 вставить новое значение по номеру" << endl
    << "10 удалить заданное значение" << endl
    << "11 удалить значение по номеру" << endl
    << endl << "Операции итератора" << endl
    << "12 установка на новое значение" << endl
    << "13 переход к след. зн." << endl
    << "14 проверка состояния" << endl
    << "15 чтение значения" << endl
    << "16 запись значения" << endl << endl;
    cout<<"0. Выход"<<endl << endl;
    cout<<"Выбор: ";
    cin >> choice;
    cout << endl<<
    "__________________________________________________________________"<< endl;
    
    
    switch (choice) {
        
            case 1:
        {
        cout << endl;
        list.show();
        cout << endl;
        }
            break;
            
            case 2:
        {
            cout << "Вы уверены, что хотите очистить список?" << endl
            << "1. Да" << endl << "2. Нет"<< endl;
            list.clear();
            cout << endl;
            cin >> secondChoice;
            switch (secondChoice)
            {
            case 1: {list.clear();} break;
            case 2: break;
            }
        }
            break;
            
            case 3:
            {
                cout << endl << "Проверка на пустоту (1 - пуст, 0 - нет): ";
                cout << list.is_empty() << endl;
            }
            break;
            
            case 4:
                        {
                            int num;
                            cout << endl << "Текущий список: " << endl;
                            list.show();
                            cout << endl << endl << "Введите значение для поиска: ";
                            cin >> num;
                            cout << endl << "Заданный вами элемент (1 - есть, 0 - нет): ";
                            cout << list.value_is_find(num) << endl;
                        }
                        break;
            case 5:
                        {
                            int index;
                            cout << endl << "Текущий список: " << endl;
                            list.show();
                            cout << endl << endl << "Введите номер элемента: ";
                            cin >> index;
                            cout << endl << "Ваш элемент по номеру " << index << " : ";
                            try
                            {
                                cout << list.value_of_position(index);
                            } catch(...){cout<<"Ошибка"<<endl;}
                            cout << endl;
            
                        }
                        break;

            case 6:
                        {
                            int index, num;
                            cout << endl << "Текущий список: " << endl;
                            list.show();
                            cout << endl << endl << "Введите номер элемента, который хотите изменить:";
                            cin >> index;
                            cout << endl << "Введите новое значение: ";
                            cin >> num;
                            bool t=list.change_value(index, num);
                            cout<<"Операция прошла успешно? (1 - да, 0 - нет): "<<t<<endl;
                            cout << endl << "Новый список: " << endl;
                            list.show();
                            cout << endl;
            
                        }
                        break;
            case 7:
            {
                int num;
                cout << endl << "Текущий список: " << endl;
                list.show();
                cout << endl << endl << "Введите значение элемента, позицию которого хотите получить: ";
                cin >> num;
                cout << endl << "Позиция элемента " << num << " : ";
                cout << list.get_position(num);
                cout << endl;
            }
            break;

            case 8:
                {
                    int num;
                    cout << endl << "Введите новое значение: ";
                    cin >> num;
                    list.push_back(num);
                    cout << endl << "Текущий список: " << endl;
                    list.show();
                    cout << endl;
                }
                break;
            
            case 9:
                {
                    int num, pos;
                    cout << endl << "Текущий список: " << endl;
                    list.show();
                    cout << endl << endl << "Введите новое значение: ";
                    cin >> num;
                    cout << endl << "Введите позицию: ";
                    cin >> pos;
                    list.push_at_position(pos, num);
                    cout<<"Операция прошла успешно? (1 - да, 0 - нет): "<<endl;
                    cout << endl << "Новый список: " << endl;
                    list.show();
                    cout << endl;
                }
                break;

            case 10:
                {
                    int num;
                    cout << endl << "Текущий список: " << endl;
                    list.show();
                    cout << endl << endl << "Введите удаляемое значение: ";
                    cin >> num;
                    list.del_at_value(num);
                    cout << endl << "Новый список: " << endl;
                    list.show();
                    cout << endl;
                }
                break;

            case 11:
                {
                    int pos;
                    cout << endl << "Текущий список: " << endl;
                    list.show();
                    cout << endl << endl << "Введите номер удаляемого элемента: ";
                    cin >> pos;
                    list.del_at_position(pos);
                    cout<<"Операция прошла успешно? (1 - да, 0 - нет): "<<endl;
                    cout << endl << "Новый список: " << endl;
                    list.show();
                    cout << endl;
                }
                break;
        
            case 0:
            break;
    }
    
    }
    
    while (choice != 0);
}
